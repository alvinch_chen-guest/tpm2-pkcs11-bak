LIB_PKCS11_C = src/pkcs11.c 
LIB_PKCS11_H = src/pkcs11.h 
LIB_PKCS11_SRC = $(LIB_PKCS11_C) $(LIB_PKCS11_H)
LIB_PKCS11_INTERNAL_LIB_C = src/lib/attrs.c src/lib/db.c src/lib/digest.c src/lib/emitter.c src/lib/encrypt.c src/lib/general.c src/lib/key.c src/lib/mech.c src/lib/mutex.c src/lib/object.c src/lib/openssl_compat.c src/lib/parser.c src/lib/random.c src/lib/session.c src/lib/session_ctx.c src/lib/session_table.c src/lib/sign.c src/lib/slot.c src/lib/token.c src/lib/tpm.c src/lib/twist.c src/lib/typed_memory.c src/lib/utils.c 
LIB_PKCS11_INTERNAL_LIB_H = src/lib/attrs.h src/lib/checks.h src/lib/db.h src/lib/digest.h src/lib/emitter.h src/lib/encrypt.h src/lib/general.h src/lib/key.h src/lib/list.h src/lib/log.h src/lib/mech.h src/lib/mutex.h src/lib/object.h src/lib/openssl_compat.h src/lib/parser.h src/lib/pkcs11f.h src/lib/pkcs11t.h src/lib/random.h src/lib/session.h src/lib/session_ctx.h src/lib/session_table.h src/lib/sign.h src/lib/slot.h src/lib/token.h src/lib/tpm.h src/lib/twist.h src/lib/typed_memory.h src/lib/utils.h 
LIB_PKCS11_INTERNAL_LIB_SRC = $(LIB_PKCS11_INTERNAL_LIB_C) $(LIB_PKCS11_INTERNAL_LIB_H)
